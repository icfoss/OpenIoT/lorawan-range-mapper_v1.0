/**
 * This file is part of Wio LoRaWan Field Tester.
 *
 *   Wio LoRaWan Field Tester is free software created by Paul Pinault aka disk91. 
 *   You can redistribute it and/or modify it under the terms of the 
 *   GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   any later version.
 *
 *  Wio LoRaWan Field Tester is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Wio LoRaWan Field Tester.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  Author : Paul Pinault (disk91.com)
 */   

#ifndef __KEYS_H
#define __KEYS_H


#define __DEVEUI { 0xd0, 0xad, 0x28, 0x93, 0xa8, 0xd4, 0x72, 0x85 }
#define __APPEUI { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x22 }
#define __APPKEY { 0xFB, 0x4B, 0x0C, 0xB7, 0x83, 0x12, 0xB7, 0x73, 0x99, 0x51, 0x77, 0x33, 0x35, 0x2C, 0x23, 0xEF }
#define __ZONE 8


#endif
